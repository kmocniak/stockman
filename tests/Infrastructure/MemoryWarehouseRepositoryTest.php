<?php

namespace Tests\Stockman\Infrastructure;

use PHPUnit\Framework\TestCase;
use Stockman\Domain\Warehouse;
use Stockman\Infrastructure\MemoryWarehouseRepository;

class MemoryWarehouseRepositoryTest extends TestCase
{
    public function testRepositoryCanReturnAllStoredWarehouses()
    {
        $repository = new MemoryWarehouseRepository();

        $this->assertEmpty($repository->getAll());

        $warehouse1 = new Warehouse("Warehouse1");
        $warehouse2 = new Warehouse("Warehouse2");
        $repository->add($warehouse1);
        $repository->add($warehouse2);

        $this->assertEquals(2, count($repository->getAll()));
        $this->assertContains($warehouse1, $repository->getAll());
        $this->assertContains($warehouse2, $repository->getAll());
    }

    public function testRepositoryReturnsWarehouseWithGivenName()
    {
        $repository = new MemoryWarehouseRepository();
        $warehouseName = "Warehouse1";
        $warehouse = new Warehouse($warehouseName);
        $repository->add($warehouse);

        $this->assertSame($warehouse, $repository->getByName($warehouseName));
    }

}
