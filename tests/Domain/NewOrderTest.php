<?php
namespace Tests\Stockman\Domain;

use PHPUnit\Framework\TestCase;
use Stockman\Domain\FreightBill;
use Stockman\Domain\NewOrder;
use Stockman\Domain\Product;

class NewOrderTest extends TestCase
{
    public function testNewOrderAcceptsValidDeliveryMethod()
    {
        $newOrder = new NewOrder(FreightBill::METHOD_STANDARD, new Product('Apple', 3));
        $this->assertEquals(FreightBill::METHOD_STANDARD, $newOrder->deliveryType());
    }

    public function testNewOrderThrowsExceptionWhenNotValidDeliveryMethodIsPassed() {
        $this->expectException(\InvalidArgumentException::class);
        $newOrder = new NewOrder('totally not valid method', new Product('Apple', 3));
    }
}
