<?php
namespace Tests\Stockman\Domain;

use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Stockman\Domain\FreightBill;
use Stockman\Domain\Product;
use Stockman\Domain\Warehouse;

class WarehouseTest extends TestCase
{
    public function testWarehouseReturnsStocksForGivenProducts()
    {
        $bananas = new Product('Banana', 4);
        $oranges = new Product('Orange', 4);
        $warehouse = new Warehouse("Derphouse");
        $warehouse->addToStock("Banana", 1);
        $warehouse->addToStock("Orange", 3);
        $stock = $warehouse->getStockForProducts($bananas, $oranges);
        $this->assertEquals(1, $stock["Banana"]);
        $this->assertEquals(3, $stock["Orange"]);
    }

    public function testWarehouseStoresFreightBills()
    {
        $warehouse = new Warehouse("Derphouse");
        $freightBill = new FreightBill(Uuid::uuid4(), FreightBill::METHOD_STANDARD, new Product('bananas', 3));
        $warehouse->expectCourierWithFreightBill($freightBill);
        $this->assertSame($freightBill, $warehouse->getLatestFreightBill());
    }
}
