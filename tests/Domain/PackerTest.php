<?php
namespace Tests\Stockman\Domain;

use Stockman\Domain\Product;
use Stockman\Domain\Package;
use Stockman\Domain\Packer;
use PHPUnit\Framework\TestCase;

class PackerTest extends TestCase
{
    public function testPackerChoosesWarehouseWithSmallestStock()
    {
        $packer = new Packer();
        $optimalDeliveries = $packer->createOptimalPackages(
            [
                "A" => ["Banana" => 6],
                "B" => ["Banana" => 3]
            ],
            new Product("Banana", 3)
        );
        $this->assertEquals(
            [
                new Package("B", new Product("Banana", 3))
            ],
            $optimalDeliveries
        );
    }

    public function testPackerHandlesManyProducts()
    {
        $packer = new Packer();
        $bananas = new Product("Banana", 3);
        $oranges = new Product("Orange", 2);

        $optimalDeliveries = $packer->createOptimalPackages(
            [
                "A" => [
                    "Banana" => 6,
                    "Orange" => 3
                ],
                "B" => [
                    "Banana" => 7,
                    "Orange" => 12
                ],
            ],
            ...[
                $bananas,
                $oranges,
            ]
        );
        $this->assertEquals(
            [
                new Package("A", ...[$bananas,$oranges])
            ],
            $optimalDeliveries
        );
    }

    public function testPackerCreatesPackagesForManyWarehouses()
    {
        $packer = new Packer();
        $bananas = new Product("Banana", 3);
        $oranges = new Product("Orange", 2);
        $optimalDeliveries = $packer->createOptimalPackages(
            [
                "A" => [
                    "Banana" => 6,
                ],
                "B" => [
                    "Orange" => 12
                ],
            ],
            ...[
                $bananas,
                $oranges,
            ]
        );
        $this->assertEquals(
            [
                new Package("A", $bananas),
                new Package("B", $oranges),
            ],
            $optimalDeliveries
        );
    }
}
