<?php
namespace Tests\Stockman\Domain;

use PHPUnit\Framework\TestCase;
use Stockman\Domain\FreightBill;
use Stockman\Domain\Product;
use Stockman\Domain\NewOrder;
use Stockman\Domain\Packer;
use Stockman\Domain\Stockman;
use Stockman\Domain\Warehouse;
use Stockman\Infrastructure\CourierStub;
use Stockman\Infrastructure\MemoryWarehouseRepository;

class StockmanTest extends TestCase
{
    public function testStockmanProcessesNewDelivery()
    {
        $deliveryType = FreightBill::METHOD_STANDARD;
        $product = new Product("Banana", 5);
        $newOrderRequest = new NewOrder($deliveryType, $product);

        $warehouseRepository = new MemoryWarehouseRepository();
        $warehouse = new Warehouse("WarehouseName");
        $warehouseRepository->add($warehouse);
        $warehouse->addToStock('Banana', 10);
        $courier = new CourierStub();
        $stockman = new Stockman($warehouseRepository, $courier, new Packer());

        $stockman->processNewOrder($newOrderRequest);

        $freightBill = $warehouse->getLatestFreightBill();

        $this->assertEquals($deliveryType, $freightBill->deliveryMethod());
        $this->assertEquals([$product], $freightBill->products());
    }
}
