<?php
namespace Stockman\Infrastructure;

use Stockman\Domain\Warehouse;
use Stockman\Domain\WarehouseRepository;

class MemoryWarehouseRepository implements WarehouseRepository
{
    private $warehouses;

    public function __construct()
    {
        $this->warehouses = [];
    }

    public function getAll(): array
    {
        return $this->warehouses;
    }

    public function add(Warehouse $warehouse)
    {
        $this->warehouses[] = $warehouse;
    }

    public function getByName(string $warehouseName): Warehouse
    {
        /** @var Warehouse $warehouse */
        foreach ($this->warehouses as $warehouse) {
            if ($warehouse->name() == $warehouseName) {
                return $warehouse;
            }
        }
        throw new \RuntimeException("No warehouse found with name " . $warehouseName);
    }
}