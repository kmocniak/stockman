<?php
namespace Stockman\Infrastructure;

use Ramsey\Uuid\Uuid;
use Stockman\Domain\Courier;
use Stockman\Domain\Package;
use Stockman\Domain\FreightBill;

class CourierStub implements Courier
{
    public function createFreightBill(Package $package, string $deliveryMethod): FreightBill
    {
        return new FreightBill(Uuid::uuid4(), $deliveryMethod, ...$package->contents());
    }
}