<?php

namespace Stockman\Domain;

interface Courier
{
    public function createFreightBill(Package $package, string $deliveryMethod): FreightBill;
}