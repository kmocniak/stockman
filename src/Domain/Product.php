<?php
namespace Stockman\Domain;

class Product
{
    private $name;
    private $quantity;

    public function __construct(string $name, int $quantity)
    {

        $this->name = $name;
        $this->quantity = $quantity;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }

}