<?php
namespace Stockman\Domain;

class Warehouse
{
    private $name;
    private $stock;
    private $freightBills;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->stock = [];
        $this->freightBills = [];
    }
    public function name(): string
    {
        return $this->name;
    }

    public function getStockForProducts(Product ...$products): array
    {
        $stock = [];
        foreach ($products as $product) {
            if (empty($this->stock[$product->name()])) {
                $stock[$product->name()] = 0;
            }
            else {
                $stock[$product->name()] = $this->stock[$product->name()];
            }
        }
        return $stock;
    }

    public function expectCourierWithFreightBill(FreightBill $freightBill)
    {
        $this->freightBills[] = $freightBill;
    }

    public function addToStock(string $productName, int $quantity)
    {
        if (isset($this->stock[$productName])) {
            $this->stock[$productName] += $quantity;
        }
        $this->stock[$productName] = $quantity;
    }

    public function getLatestFreightBill(): FreightBill
    {
        return end($this->freightBills);
    }
}