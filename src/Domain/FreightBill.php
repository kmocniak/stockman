<?php
namespace Stockman\Domain;

use Ramsey\Uuid\UuidInterface;

class FreightBill
{
    const METHOD_STANDARD = 'standard';
    const METHOD_FAST = 'fast';
    const METHOD_EXPRESS = 'express';
    const DELIVERY_METHODS = [
        self::METHOD_STANDARD,
        self::METHOD_FAST,
        self::METHOD_EXPRESS,
    ];

    private $items;
    private $deliveryMethod;
    private $deliveryId;

    public function __construct(UuidInterface $deliveryId, string $deliveryMethod, Product ...$items)
    {
        $this->items = $items;
        $this->deliveryMethod = $deliveryMethod;
        $this->deliveryId = $deliveryId;
    }

    /** @return Product[] */
    public function products(): array
    {
        return $this->items;
    }

    public function deliveryMethod(): string
    {
        return $this->deliveryMethod;
    }
}