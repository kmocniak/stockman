<?php

namespace Stockman\Domain;

class Packer
{
    public function createOptimalPackages($stocks, Product ...$products): array
    {
        $deliveries = [];
        /** @var Product $product */
        foreach ($products as $product) {
            $warehouseWithMinimalStock = null;
            $minimalStock = null;
            foreach ($stocks as $warehouseName => $stock) {
                if (isset($stock[$product->name()])) {
                    $availableQuantity = $stock[$product->name()];
                    if ($availableQuantity >= $product->quantity()) {
                        if (null === $minimalStock || $availableQuantity < $minimalStock) {
                            $minimalStock = $availableQuantity;
                            $warehouseWithMinimalStock = $warehouseName;
                        }
                    }
                }
            }
            if (null === $warehouseWithMinimalStock) throw new \RuntimeException("Product " . $product->name() . " not in stock.");
            $deliveries[$warehouseWithMinimalStock][] = $product;
        }
        $objects = [];
        foreach ($deliveries as $warehouseName => $contents) {
            $objects[] = new Package($warehouseName, ...$contents);
        }
        return $objects;
    }
}