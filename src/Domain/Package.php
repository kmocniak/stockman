<?php

namespace Stockman\Domain;

class Package
{
    private $warehouseName;
    private $products;

    public function __construct(string $warehouseName, Product ...$products)
    {
        $this->warehouseName = $warehouseName;
        $this->products = $products;
    }

    public function warehouseName(): string
    {
        return $this->warehouseName;
    }

    public function contents(): array
    {
        return $this->products;
    }
}