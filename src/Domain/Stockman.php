<?php

namespace Stockman\Domain;

class Stockman
{
    private $warehouseRepository;
    /**
     * @var Courier
     */
    private $courier;
    /**
     * @var Packer
     */
    private $packer;

    public function __construct(WarehouseRepository $warehouseRepository, Courier $courier, Packer $packer)
    {
        $this->warehouseRepository = $warehouseRepository;
        $this->courier = $courier;
        $this->packer = $packer;
    }

    public function processNewOrder(NewOrder $newOrder)
    {
        /** @var Warehouse[] $warehouses */
        $warehouses = $this->warehouseRepository->getAll();

        $stocks = [];
        foreach ($warehouses as $warehouse) {
            $stocks[$warehouse->name()] = $warehouse->getStockForProducts(...$newOrder->products());
        }

        $packages = $this->packer->createOptimalPackages($stocks, ...$newOrder->products());

        /** @var Package $package */
        foreach ($packages as $package) {
            $freightBill = $this->courier->createFreightBill($package, $newOrder->deliveryType());
            $warehouse = $this->warehouseRepository->getByName($package->warehouseName());
            $warehouse->expectCourierWithFreightBill($freightBill);
        }
    }
}