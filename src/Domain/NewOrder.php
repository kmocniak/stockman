<?php

namespace Stockman\Domain;

class NewOrder
{
    private $deliveryType;
    private $items;

    public function __construct(string $deliveryType, Product ...$items)
    {
        if (!in_array($deliveryType, FreightBill::DELIVERY_METHODS)) {
            throw new \InvalidArgumentException($deliveryType . ' is not valid delivery method');
        }
        $this->deliveryType = $deliveryType;
        $this->items = $items;
    }

    public function deliveryType(): string
    {
        return $this->deliveryType;
    }

    /** @return Product[] */
    public function products(): array
    {
        return $this->items;
    }
}