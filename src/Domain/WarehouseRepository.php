<?php
namespace Stockman\Domain;

interface WarehouseRepository
{
    public function getAll(): array;

    public function getByName(string $warehouseName): Warehouse;

    public function add(Warehouse $warehouse);
}