# Stockman

Simple integration between warehouse and courier company


### Installing
After clone repository and cd into project directory:

```
$ composer install
```
## Running the tests
Unit tests:
```
$ vendor/bin/phpunit 
```
Behat:
```
$ vendor/bin/behat
```

